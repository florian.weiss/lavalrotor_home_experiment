import time
import h5py
import adafruit_adxl34x
import numpy as np
import board
import json
import os

from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting

"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start
# TODO: Adjust the parameters to your needs
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
path_setup_json = "datasheets/setup_mixer.json"  # adjust this to the setup json path
measure_duration_in_s = 20

# ---------------------------------------------------------------------------------------------#1-end

"""Prepare Metadata and create H5-File"""
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="datasheets")

print("Setup dictionary:")
print(json.dumps(setup_json_dict, indent=2, default=str))
print()
print("Sensor settings dictionary")
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


"""Establishing a connection to the acceleration sensor"""
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)


# -------------------------------------------------------------------------------------------#2-start
# TODO: Initialize the data structure
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # get uuid from dictionary
keys = [] # create list for keys of setup dictionary
for key in setup_json_dict: # save all keys in setup dictionary to list
    keys.append(key)
uuid_sensor = keys[3] # thats the uuid of the sensor

    # create data dictionary
data_sensor = {
    uuid_sensor: { # key of dictionary is uuid 
        'acceleration_x': [], # empty acceleration lists
        'acceleration_y': [],
        'acceleration_z': [],
        'timestamp': [] # empty timestamp list
    }
}


# ---------------------------------------------------------------------------------------------#2-end


# -------------------------------------------------------------------------------------------#3-start
# TODO: Measure the probe
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # setup measurement
    # initialization (from documentation)
i2c = board.I2C()  # uses board.SCL and board.SDA
accelerometer = adafruit_adxl34x.ADXL345(i2c)

t_start = time.time() # an initial time is needed to track duration of measurement

    # measurement
while time.time() - t_start <= measure_duration_in_s: # for measurement duration...
    t_current = time.time() - t_start #...save current time immediately for accuracy...
    data_sensor[uuid_sensor]['acceleration_x'].append(accelerometer.acceleration[0]) #...save x acceleration...
    data_sensor[uuid_sensor]['acceleration_y'].append(accelerometer.acceleration[1]) #...save y acceleration...
    data_sensor[uuid_sensor]['acceleration_z'].append(accelerometer.acceleration[2]) #...save z acceleration...
    data_sensor[uuid_sensor]['timestamp'].append(t_current) #...and save the time.
    time.sleep(0.001) # pause for 1ms

# ---------------------------------------------------------------------------------------------#3-end

# -------------------------------------------------------------------------------------------#4-start
# TODO: Write results in hdf5
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # initialize h5 file
with h5py.File(path_h5_file, 'w') as file:
    group = file.create_group(uuid_sensor) # create group with sensor uuid as name
    
    # fill the h5 file with the data
        #initialization
    units = ['m/s^2', 'm/s^2','m/s^2', 's'] # a list of units to add as attributes
    i = 0 # counter to check when to use which unit
    
        # 
    for key in data_sensor[uuid_sensor].keys(): # for all keys in the dictionary...
        dataset = group.create_dataset(key, data = data_sensor[uuid_sensor][key]) #...put data into h5 structure...
        dataset.attrs['unit'] = units[i] #...and add the unit.
        i = i+1 # counter for the units

    

# ---------------------------------------------------------------------------------------------#4-end

"""Log JSON metadata"""
log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
print("Measurement data was saved in {}/".format(path_measurement_folder))
